/********************************************************************************
** Form generated from reading UI file 'mainwindow.ui'
**
** Created: Thu Feb 27 00:05:07 2014
**      by: Qt User Interface Compiler version 4.7.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_MAINWINDOW_H
#define UI_MAINWINDOW_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QFrame>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QStatusBar>
#include <QtGui/QToolBar>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_MainWindow
{
public:
    QAction *actionExit;
    QAction *actionStart_Wizard_Application;
    QAction *actionStart_Wizard_Simulation;
    QAction *actionSave_Wizard_Configuration;
    QAction *actionAdd_Are;
    QAction *actionConfig;
    QAction *actionSave_Map;
    QAction *actionSet_DIsplay;
    QAction *actionAdjust_Brightnes;
    QAction *actionAdjust_Brightnes_2;
    QAction *actionColour_Display_Area;
    QAction *actionConfiguration;
    QAction *actionStart_Tracking;
    QAction *actionEstimation_Control;
    QAction *actionAbout;
    QAction *actionDocumentation;
    QWidget *centralWidget;
    QLabel *label;
    QLabel *label_2;
    QLabel *label_3;
    QLabel *label_4;
    QLineEdit *lineEdit;
    QLineEdit *lineEdit_2;
    QLineEdit *lineEdit_3;
    QLabel *label_5;
    QLabel *label_6;
    QLabel *label_7;
    QLineEdit *lineEdit_4;
    QLabel *label_8;
    QLineEdit *lineEdit_5;
    QLineEdit *lineEdit_6;
    QLabel *label_11;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QPushButton *pushButton_3;
    QLabel *label_9;
    QFrame *frame;
    QFrame *frame_2;
    QFrame *frame_3;
    QFrame *frame_4;
    QFrame *frame_5;
    QLabel *label_10;
    QPushButton *pushButton_4;
    QPushButton *pushButton_5;
    QPushButton *pushButton_6;
    QPushButton *pushButton_7;
    QFrame *frame_6;
    QLabel *label_12;
    QFrame *frame_7;
    QMenuBar *menuBar;
    QMenu *menuFIle;
    QMenu *menuProcess;
    QMenu *menuMap;
    QMenu *menuDisplay;
    QMenu *menuTracking;
    QMenu *menuHelp;
    QToolBar *mainToolBar;
    QStatusBar *statusBar;

    void setupUi(QMainWindow *MainWindow)
    {
        if (MainWindow->objectName().isEmpty())
            MainWindow->setObjectName(QString::fromUtf8("MainWindow"));
        MainWindow->resize(845, 502);
        actionExit = new QAction(MainWindow);
        actionExit->setObjectName(QString::fromUtf8("actionExit"));
        actionStart_Wizard_Application = new QAction(MainWindow);
        actionStart_Wizard_Application->setObjectName(QString::fromUtf8("actionStart_Wizard_Application"));
        actionStart_Wizard_Simulation = new QAction(MainWindow);
        actionStart_Wizard_Simulation->setObjectName(QString::fromUtf8("actionStart_Wizard_Simulation"));
        actionSave_Wizard_Configuration = new QAction(MainWindow);
        actionSave_Wizard_Configuration->setObjectName(QString::fromUtf8("actionSave_Wizard_Configuration"));
        actionAdd_Are = new QAction(MainWindow);
        actionAdd_Are->setObjectName(QString::fromUtf8("actionAdd_Are"));
        actionConfig = new QAction(MainWindow);
        actionConfig->setObjectName(QString::fromUtf8("actionConfig"));
        actionSave_Map = new QAction(MainWindow);
        actionSave_Map->setObjectName(QString::fromUtf8("actionSave_Map"));
        actionSet_DIsplay = new QAction(MainWindow);
        actionSet_DIsplay->setObjectName(QString::fromUtf8("actionSet_DIsplay"));
        actionAdjust_Brightnes = new QAction(MainWindow);
        actionAdjust_Brightnes->setObjectName(QString::fromUtf8("actionAdjust_Brightnes"));
        actionAdjust_Brightnes_2 = new QAction(MainWindow);
        actionAdjust_Brightnes_2->setObjectName(QString::fromUtf8("actionAdjust_Brightnes_2"));
        actionColour_Display_Area = new QAction(MainWindow);
        actionColour_Display_Area->setObjectName(QString::fromUtf8("actionColour_Display_Area"));
        actionConfiguration = new QAction(MainWindow);
        actionConfiguration->setObjectName(QString::fromUtf8("actionConfiguration"));
        actionStart_Tracking = new QAction(MainWindow);
        actionStart_Tracking->setObjectName(QString::fromUtf8("actionStart_Tracking"));
        actionEstimation_Control = new QAction(MainWindow);
        actionEstimation_Control->setObjectName(QString::fromUtf8("actionEstimation_Control"));
        actionAbout = new QAction(MainWindow);
        actionAbout->setObjectName(QString::fromUtf8("actionAbout"));
        actionDocumentation = new QAction(MainWindow);
        actionDocumentation->setObjectName(QString::fromUtf8("actionDocumentation"));
        centralWidget = new QWidget(MainWindow);
        centralWidget->setObjectName(QString::fromUtf8("centralWidget"));
        label = new QLabel(centralWidget);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(20, 0, 91, 17));
        label->setText(QString::fromUtf8("STATUS"));
        label->setTextFormat(Qt::LogText);
        label_2 = new QLabel(centralWidget);
        label_2->setObjectName(QString::fromUtf8("label_2"));
        label_2->setGeometry(QRect(20, 30, 91, 17));
        label_3 = new QLabel(centralWidget);
        label_3->setObjectName(QString::fromUtf8("label_3"));
        label_3->setGeometry(QRect(20, 50, 81, 16));
        label_4 = new QLabel(centralWidget);
        label_4->setObjectName(QString::fromUtf8("label_4"));
        label_4->setGeometry(QRect(20, 70, 91, 16));
        lineEdit = new QLineEdit(centralWidget);
        lineEdit->setObjectName(QString::fromUtf8("lineEdit"));
        lineEdit->setGeometry(QRect(120, 30, 111, 16));
        lineEdit_2 = new QLineEdit(centralWidget);
        lineEdit_2->setObjectName(QString::fromUtf8("lineEdit_2"));
        lineEdit_2->setGeometry(QRect(120, 50, 111, 16));
        lineEdit_3 = new QLineEdit(centralWidget);
        lineEdit_3->setObjectName(QString::fromUtf8("lineEdit_3"));
        lineEdit_3->setGeometry(QRect(120, 70, 111, 16));
        label_5 = new QLabel(centralWidget);
        label_5->setObjectName(QString::fromUtf8("label_5"));
        label_5->setGeometry(QRect(20, 180, 91, 16));
        label_6 = new QLabel(centralWidget);
        label_6->setObjectName(QString::fromUtf8("label_6"));
        label_6->setGeometry(QRect(20, 140, 91, 17));
        label_7 = new QLabel(centralWidget);
        label_7->setObjectName(QString::fromUtf8("label_7"));
        label_7->setGeometry(QRect(20, 110, 111, 20));
        label_7->setText(QString::fromUtf8("INPUT SOURCE"));
        label_7->setTextFormat(Qt::LogText);
        lineEdit_4 = new QLineEdit(centralWidget);
        lineEdit_4->setObjectName(QString::fromUtf8("lineEdit_4"));
        lineEdit_4->setGeometry(QRect(120, 180, 111, 16));
        lineEdit_4->setText(QString::fromUtf8(""));
        label_8 = new QLabel(centralWidget);
        label_8->setObjectName(QString::fromUtf8("label_8"));
        label_8->setGeometry(QRect(20, 160, 81, 16));
        lineEdit_5 = new QLineEdit(centralWidget);
        lineEdit_5->setObjectName(QString::fromUtf8("lineEdit_5"));
        lineEdit_5->setGeometry(QRect(120, 140, 111, 16));
        lineEdit_5->setText(QString::fromUtf8(""));
        lineEdit_6 = new QLineEdit(centralWidget);
        lineEdit_6->setObjectName(QString::fromUtf8("lineEdit_6"));
        lineEdit_6->setGeometry(QRect(120, 160, 111, 16));
        lineEdit_6->setText(QString::fromUtf8(""));
        label_11 = new QLabel(centralWidget);
        label_11->setObjectName(QString::fromUtf8("label_11"));
        label_11->setGeometry(QRect(20, 230, 111, 20));
        label_11->setText(QString::fromUtf8("Documentation"));
        label_11->setTextFormat(Qt::LogText);
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));
        pushButton->setGeometry(QRect(20, 260, 121, 27));
        pushButton_2 = new QPushButton(centralWidget);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));
        pushButton_2->setGeometry(QRect(20, 300, 121, 27));
        pushButton_3 = new QPushButton(centralWidget);
        pushButton_3->setObjectName(QString::fromUtf8("pushButton_3"));
        pushButton_3->setGeometry(QRect(20, 340, 121, 27));
        label_9 = new QLabel(centralWidget);
        label_9->setObjectName(QString::fromUtf8("label_9"));
        label_9->setGeometry(QRect(270, 0, 81, 17));
        frame = new QFrame(centralWidget);
        frame->setObjectName(QString::fromUtf8("frame"));
        frame->setGeometry(QRect(270, 30, 401, 401));
        frame->setFrameShape(QFrame::StyledPanel);
        frame->setFrameShadow(QFrame::Raised);
        frame_2 = new QFrame(centralWidget);
        frame_2->setObjectName(QString::fromUtf8("frame_2"));
        frame_2->setGeometry(QRect(10, 0, 231, 91));
        frame_2->setFrameShape(QFrame::StyledPanel);
        frame_2->setFrameShadow(QFrame::Raised);
        frame_3 = new QFrame(centralWidget);
        frame_3->setObjectName(QString::fromUtf8("frame_3"));
        frame_3->setGeometry(QRect(10, 110, 231, 91));
        frame_3->setFrameShape(QFrame::StyledPanel);
        frame_3->setFrameShadow(QFrame::Raised);
        frame_4 = new QFrame(centralWidget);
        frame_4->setObjectName(QString::fromUtf8("frame_4"));
        frame_4->setGeometry(QRect(10, 220, 231, 161));
        frame_4->setFrameShape(QFrame::StyledPanel);
        frame_4->setFrameShadow(QFrame::Raised);
        frame_5 = new QFrame(centralWidget);
        frame_5->setObjectName(QString::fromUtf8("frame_5"));
        frame_5->setGeometry(QRect(690, 0, 141, 201));
        frame_5->setFrameShape(QFrame::StyledPanel);
        frame_5->setFrameShadow(QFrame::Raised);
        label_10 = new QLabel(frame_5);
        label_10->setObjectName(QString::fromUtf8("label_10"));
        label_10->setGeometry(QRect(20, 0, 111, 17));
        pushButton_4 = new QPushButton(frame_5);
        pushButton_4->setObjectName(QString::fromUtf8("pushButton_4"));
        pushButton_4->setGeometry(QRect(20, 30, 98, 27));
        pushButton_5 = new QPushButton(frame_5);
        pushButton_5->setObjectName(QString::fromUtf8("pushButton_5"));
        pushButton_5->setGeometry(QRect(20, 70, 98, 27));
        pushButton_6 = new QPushButton(frame_5);
        pushButton_6->setObjectName(QString::fromUtf8("pushButton_6"));
        pushButton_6->setGeometry(QRect(20, 110, 98, 27));
        pushButton_7 = new QPushButton(frame_5);
        pushButton_7->setObjectName(QString::fromUtf8("pushButton_7"));
        pushButton_7->setGeometry(QRect(20, 150, 98, 27));
        frame_6 = new QFrame(centralWidget);
        frame_6->setObjectName(QString::fromUtf8("frame_6"));
        frame_6->setGeometry(QRect(690, 220, 141, 211));
        frame_6->setFrameShape(QFrame::StyledPanel);
        frame_6->setFrameShadow(QFrame::Raised);
        label_12 = new QLabel(frame_6);
        label_12->setObjectName(QString::fromUtf8("label_12"));
        label_12->setGeometry(QRect(20, 10, 101, 17));
        frame_7 = new QFrame(frame_6);
        frame_7->setObjectName(QString::fromUtf8("frame_7"));
        frame_7->setGeometry(QRect(10, 30, 121, 171));
        frame_7->setFrameShape(QFrame::StyledPanel);
        frame_7->setFrameShadow(QFrame::Raised);
        MainWindow->setCentralWidget(centralWidget);
        menuBar = new QMenuBar(MainWindow);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 845, 25));
        menuFIle = new QMenu(menuBar);
        menuFIle->setObjectName(QString::fromUtf8("menuFIle"));
        menuProcess = new QMenu(menuBar);
        menuProcess->setObjectName(QString::fromUtf8("menuProcess"));
        menuMap = new QMenu(menuBar);
        menuMap->setObjectName(QString::fromUtf8("menuMap"));
        menuDisplay = new QMenu(menuBar);
        menuDisplay->setObjectName(QString::fromUtf8("menuDisplay"));
        menuTracking = new QMenu(menuBar);
        menuTracking->setObjectName(QString::fromUtf8("menuTracking"));
        menuHelp = new QMenu(menuBar);
        menuHelp->setObjectName(QString::fromUtf8("menuHelp"));
        MainWindow->setMenuBar(menuBar);
        mainToolBar = new QToolBar(MainWindow);
        mainToolBar->setObjectName(QString::fromUtf8("mainToolBar"));
        MainWindow->addToolBar(Qt::TopToolBarArea, mainToolBar);
        statusBar = new QStatusBar(MainWindow);
        statusBar->setObjectName(QString::fromUtf8("statusBar"));
        MainWindow->setStatusBar(statusBar);

        menuBar->addAction(menuFIle->menuAction());
        menuBar->addAction(menuProcess->menuAction());
        menuBar->addAction(menuMap->menuAction());
        menuBar->addAction(menuDisplay->menuAction());
        menuBar->addAction(menuTracking->menuAction());
        menuBar->addAction(menuHelp->menuAction());
        menuFIle->addAction(actionExit);
        menuProcess->addAction(actionStart_Wizard_Application);
        menuProcess->addAction(actionStart_Wizard_Simulation);
        menuProcess->addAction(actionSave_Wizard_Configuration);
        menuMap->addAction(actionAdd_Are);
        menuMap->addAction(actionConfig);
        menuMap->addAction(actionSave_Map);
        menuDisplay->addAction(actionSet_DIsplay);
        menuDisplay->addAction(actionAdjust_Brightnes);
        menuDisplay->addAction(actionAdjust_Brightnes_2);
        menuDisplay->addAction(actionColour_Display_Area);
        menuTracking->addAction(actionEstimation_Control);
        menuTracking->addAction(actionStart_Tracking);
        menuTracking->addAction(actionConfiguration);
        menuHelp->addAction(actionDocumentation);
        menuHelp->addAction(actionAbout);

        retranslateUi(MainWindow);

        QMetaObject::connectSlotsByName(MainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *MainWindow)
    {
        MainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", 0, QApplication::UnicodeUTF8));
        actionExit->setText(QApplication::translate("MainWindow", "Exit", 0, QApplication::UnicodeUTF8));
        actionStart_Wizard_Application->setText(QApplication::translate("MainWindow", "Start Wizard Application", 0, QApplication::UnicodeUTF8));
        actionStart_Wizard_Simulation->setText(QApplication::translate("MainWindow", "Start Wizard Simulation", 0, QApplication::UnicodeUTF8));
        actionSave_Wizard_Configuration->setText(QApplication::translate("MainWindow", "Save Wizard Configuration", 0, QApplication::UnicodeUTF8));
        actionAdd_Are->setText(QApplication::translate("MainWindow", "Add Area", 0, QApplication::UnicodeUTF8));
        actionConfig->setText(QApplication::translate("MainWindow", "Config", 0, QApplication::UnicodeUTF8));
        actionSave_Map->setText(QApplication::translate("MainWindow", "Save Map", 0, QApplication::UnicodeUTF8));
        actionSet_DIsplay->setText(QApplication::translate("MainWindow", "Configuration", 0, QApplication::UnicodeUTF8));
        actionAdjust_Brightnes->setText(QApplication::translate("MainWindow", "Fade Control", 0, QApplication::UnicodeUTF8));
        actionAdjust_Brightnes_2->setText(QApplication::translate("MainWindow", "Adjust Brightness", 0, QApplication::UnicodeUTF8));
        actionColour_Display_Area->setText(QApplication::translate("MainWindow", "Colour Display Area", 0, QApplication::UnicodeUTF8));
        actionConfiguration->setText(QApplication::translate("MainWindow", "Option", 0, QApplication::UnicodeUTF8));
        actionStart_Tracking->setText(QApplication::translate("MainWindow", "Start Tracking", 0, QApplication::UnicodeUTF8));
        actionEstimation_Control->setText(QApplication::translate("MainWindow", "Estimation Control", 0, QApplication::UnicodeUTF8));
        actionAbout->setText(QApplication::translate("MainWindow", "About ", 0, QApplication::UnicodeUTF8));
        actionDocumentation->setText(QApplication::translate("MainWindow", "Documentation", 0, QApplication::UnicodeUTF8));
        label_2->setText(QApplication::translate("MainWindow", "Server Usage", 0, QApplication::UnicodeUTF8));
        label_3->setText(QApplication::translate("MainWindow", "Connection", 0, QApplication::UnicodeUTF8));
        label_4->setText(QApplication::translate("MainWindow", "Input Device", 0, QApplication::UnicodeUTF8));
        label_5->setText(QApplication::translate("MainWindow", "Input Device", 0, QApplication::UnicodeUTF8));
        label_6->setText(QApplication::translate("MainWindow", "Server Usage", 0, QApplication::UnicodeUTF8));
        label_8->setText(QApplication::translate("MainWindow", "Connection", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("MainWindow", "Record", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("MainWindow", "Start Broadcast", 0, QApplication::UnicodeUTF8));
        pushButton_3->setText(QApplication::translate("MainWindow", "Stop Broadcast", 0, QApplication::UnicodeUTF8));
        label_9->setText(QApplication::translate("MainWindow", "MAP AREA", 0, QApplication::UnicodeUTF8));
        label_10->setText(QApplication::translate("MainWindow", "MAP CONTROL", 0, QApplication::UnicodeUTF8));
        pushButton_4->setText(QApplication::translate("MainWindow", "Change Map", 0, QApplication::UnicodeUTF8));
        pushButton_5->setText(QApplication::translate("MainWindow", "Delete Map", 0, QApplication::UnicodeUTF8));
        pushButton_6->setText(QApplication::translate("MainWindow", "Reset Map", 0, QApplication::UnicodeUTF8));
        pushButton_7->setText(QApplication::translate("MainWindow", "New Map", 0, QApplication::UnicodeUTF8));
        label_12->setText(QApplication::translate("MainWindow", "Loging Server", 0, QApplication::UnicodeUTF8));
        menuFIle->setTitle(QApplication::translate("MainWindow", "FIle", 0, QApplication::UnicodeUTF8));
        menuProcess->setTitle(QApplication::translate("MainWindow", "Process", 0, QApplication::UnicodeUTF8));
        menuMap->setTitle(QApplication::translate("MainWindow", "Map", 0, QApplication::UnicodeUTF8));
        menuDisplay->setTitle(QApplication::translate("MainWindow", "Display", 0, QApplication::UnicodeUTF8));
        menuTracking->setTitle(QApplication::translate("MainWindow", "Tracking", 0, QApplication::UnicodeUTF8));
        menuHelp->setTitle(QApplication::translate("MainWindow", "Help", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class MainWindow: public Ui_MainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_MAINWINDOW_H
