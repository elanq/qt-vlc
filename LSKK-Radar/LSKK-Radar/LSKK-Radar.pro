#-------------------------------------------------
#
# Project created by QtCreator 2014-02-26T23:37:36
#
#-------------------------------------------------

QT       += core gui

TARGET = LSKK-Radar
TEMPLATE = app


SOURCES += main.cpp\
        mainwindow.cpp

HEADERS  += mainwindow.h

FORMS    += mainwindow.ui
