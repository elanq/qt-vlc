#-------------------------------------------------
#
# Project created by QtCreator 2014-03-04T15:46:25
#
#-------------------------------------------------

QT       += core gui

TARGET = TestVLC
TEMPLATE = app

INCLUDEPATH+= /usr/include/vlc
DEPENDPATH += /usr/include/vlc

LIBS += -L"/usr/lib" -lvlc

SOURCES += main.cpp\
        mainwindow.cpp \
    vlc_on_qt.cpp

HEADERS  += mainwindow.h \
    vlc_on_qt.h

#FORMS    += mainwindow.ui
